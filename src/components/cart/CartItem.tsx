import { View, Text, StyleProp, ViewStyle } from 'react-native';
import React from 'react';
import { CartType, ProductType } from '~containers/cart/type';
import { tw } from '~configs/tw';
import { FImage } from '~components/common';
import CheckBox from '~components/common/CheckBox';
import Quantity from './Quantity';

type Props = {
  item: CartType;
  containerStyle?: StyleProp<ViewStyle>;
  onChange: (data: any) => void;
  value: any;
};

const CartItem = ({ item, containerStyle, onChange, value }: Props) => {
  const { store, products } = item || {};

  const handleSelectStore = (val: boolean) => {
    const newValue = products?.reduce?.((acc: any, cur) => {
      acc[cur?.id] = {
        selected: val,
        quantity: value?.[cur?.id]?.quantity || 1,
      };
      return acc;
    }, {});

    onChange(newValue);
  };

  const onHandleSelectProduct = (product: ProductType) => (val: boolean) => {
    onChange({
      ...value,
      [product?.id]: {
        ...value?.[product?.id],
        selected: val,
      },
    });
  };

  const onChangeQuantity = (productId: number) => (val: number) => {
    onChange({
      ...value,
      [productId]: {
        ...value?.[productId],
        quantity: val,
      },
    });
  };

  const renderProduct = (product: ProductType) => {
    return (
      <View style={tw`flex-row mt-2`} key={product?.id}>
        <View style={tw`self-center`}>
          <CheckBox selected={value?.[product?.id]?.selected} onSelect={onHandleSelectProduct(product)} />
        </View>
        <FImage source={{ uri: product?.image }} containerStyle={tw`w-20 h-20 mx-2`} />
        <View style={tw`flex-1 justify-between`}>
          <View>
            <Text style={tw`text-16px`}>{product?.name}</Text>
            <Text style={tw`text-3`}>{product?.subName}</Text>
          </View>

          <View style={tw`flex-row justify-between`}>
            <Text style={tw`text-red-500`}>
              ${product?.discount} <Text style={tw`text-black text-10px line-through`}>${product?.price}</Text>
            </Text>
            <Quantity onDec={onChangeQuantity(product?.id)} onInc={onChangeQuantity(product?.id)} />
          </View>
        </View>
      </View>
    );
  };

  return (
    <View style={[tw`mx-4 mt-4`, containerStyle]}>
      <View style={tw`flex-row items-center`}>
        <CheckBox
          selected={Object.values(value || {})?.filter?.((e: any) => e?.selected).length === products?.length}
          onSelect={handleSelectStore}
        />
        <View style={tw`flex-1 mx-2`}>
          <Text style={tw`font-medium`}>{store?.name}</Text>
        </View>
        <Text style={tw`text-3`}>{'View store >'}</Text>
      </View>

      {products?.map?.(renderProduct)}
    </View>
  );
};

export default CartItem;
