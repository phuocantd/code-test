import { View, Text, TouchableOpacity } from 'react-native';
import React, { useState } from 'react';
import { tw } from '~configs/tw';

type Props = {
  onDec: (val: number) => void;
  onInc: (val: number) => void;
};

const Quantity = ({ onDec, onInc }: Props) => {
  const [count, setCount] = useState(1);

  const handleDec = () => {
    if (count > 1) {
      const val = count - 1;
      setCount(val);
      onDec(val);
    }
  };

  const handleInc = () => {
    const val = count + 1;
    setCount(val);
    onInc(val);
  };

  return (
    <View style={tw`flex-row items-center`}>
      <TouchableOpacity style={tw`bg-gray-300 w-5 h-5 justify-center items-center`} onPress={handleDec}>
        <Text>-</Text>
      </TouchableOpacity>
      <Text style={tw`w-5 h-5 text-center`}>{count}</Text>
      <TouchableOpacity style={tw`bg-gray-300 w-5 h-5 justify-center items-center`} onPress={handleInc}>
        <Text>+</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Quantity;
