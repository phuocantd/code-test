import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { tw } from '~configs/tw';

type Props = {
  selected: boolean;
  onSelect: (val: boolean) => void;
};

const CheckBox = ({ selected, onSelect }: Props) => {
  const handleSelect = () => {
    onSelect(!selected);
  };

  return (
    <TouchableOpacity style={tw`w-4 h-4 border items-center justify-center`} onPress={handleSelect}>
      {selected && <View style={tw`w-3 h-3 bg-black`} />}
    </TouchableOpacity>
  );
};

export default CheckBox;
