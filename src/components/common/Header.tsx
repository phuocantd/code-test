import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { BackSVG } from '~assets';
import { tw } from '~configs/tw';

type Props = {
  title: string;
  onBack: () => void;
};

const Header = ({ title, onBack }: Props) => {
  return (
    <View style={tw`flex-row mx-4 mb-2`}>
      <TouchableOpacity onPress={onBack}>
        <BackSVG width={20} height={20} />
      </TouchableOpacity>
      <View style={tw`flex-1`}>
        <Text style={tw`text-center`}>{title}</Text>
      </View>
      <Text style={tw`text-20px`}>...</Text>
    </View>
  );
};

export default Header;
