import { useNavigation } from '@react-navigation/native';
import React, { useMemo, useState } from 'react';
import { FlatList, Text, TouchableOpacity, View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Header } from '~components';
import { CartItem } from '~components/cart';
import { tw } from '~configs/tw';
import { useDidMount } from '~hooks';
import { CartType } from './type';

const CartScreen = () => {
  const navigation = useNavigation();

  const [selected, setSelected] = useState<any>({});

  // Tính số lượng sản phẩm trong giỏ hàng
  const quantity = useMemo(() => DATA.reduce((acc, cur) => acc + cur?.products?.length, 0), []);
  // Chuyển object thành array
  const selectedToArray = useMemo(
    () =>
      Object.keys(selected)
        .map?.((e: string) => {
          return Object.values(selected[e]);
        })
        .flat(),
    [selected],
  );

  // tính tổng tiền
  const itemTotal = useMemo(() => {
    return selectedToArray?.reduce?.(
      (acc: number, cur: any) => acc + (cur?.selected ? cur?.quantity * cur?.product?.price : 0),
      0,
    );
  }, [selectedToArray]);
  // tính tổng tiền giảm giá
  const itemDiscount = useMemo(() => {
    return selectedToArray?.reduce?.(
      (acc: number, cur: any) => acc + (cur?.selected ? cur?.quantity * cur?.product?.discount : 0),
      0,
    );
  }, [selectedToArray]);
  // tính số lượng sản phẩm được chọn
  const total = useMemo(
    () => selectedToArray?.reduce((acc: number, cur: any) => acc + (cur?.selected ? cur?.quantity : 0), 0),
    [selectedToArray],
  );

  useDidMount(() => {
    setSelected(
      DATA.reduce?.((acc: any, cur) => {
        acc[cur?.store?.id] = cur?.products?.reduce?.((acc2: any, cur2) => {
          acc2[cur2.id] = {
            selected: false,
            quantity: 1,
            product: cur2,
          };
          return acc2;
        }, {});
        return acc;
      }, {}),
    );
  });

  const renderHeader = () => (
    <View>
      <Text style={tw`font-medium mx-4`}>All ({quantity} items)</Text>
    </View>
  );

  const onChange = (storeId: number) => (data: any) => {
    setSelected({
      ...selected,
      [storeId]: data,
    });
  };

  const renderItem = ({ item }: { item: CartType }) => (
    <CartItem item={item} value={selected?.[item?.id]} onChange={onChange(item?.store?.id)} />
  );

  return (
    <SafeAreaView edges={['top', 'bottom']} style={tw`flex-1`}>
      <Header title="Shopping cart" onBack={() => navigation.goBack()} />
      <FlatList data={DATA} renderItem={renderItem} ListHeaderComponent={renderHeader} />
      <View>
        <View style={tw`flex-row justify-between mx-4`}>
          <Text>Item total</Text>
          <Text style={tw`line-through`}>${itemTotal.toFixed(2)}</Text>
        </View>
        <View style={tw`flex-row justify-between mx-4`}>
          <Text>Item discount</Text>
          <Text style={tw`text-red-500`}>${itemDiscount.toFixed(2)}</Text>
        </View>
        <View style={tw`flex-row justify-between mx-4`}>
          <Text>Subtotal</Text>
          <Text style={tw``}>${itemDiscount.toFixed(2)}</Text>
        </View>
        <View style={tw`flex-row justify-between mx-4`}>
          <Text>Item discount</Text>
          <Text style={tw``}>${itemTotal.toFixed(2)}</Text>
        </View>
        <View style={tw`flex-row justify-between mx-4`}>
          <Text style={tw`font-medium`}>Estimated total</Text>
          <Text style={tw`font-medium`}>${itemTotal.toFixed(2)}</Text>
        </View>

        <View style={tw`m-4 flex-row justify-between items-center gap-4`}>
          <View>
            <Text>Est total {total}</Text>
            <Text>${itemTotal}</Text>
          </View>
          <View style={tw`flex-1`}>
            <TouchableOpacity style={tw` bg-pink-400 items-center justify-center rounded-1 py-2`}>
              <Text style={tw`text-white`}>Checkout</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const DATA: CartType[] = [
  {
    id: 1,
    store: {
      id: 1,
      name: 'ABC Nail Supply',
    },
    products: [
      {
        id: 1,
        name: 'Product 1',
        image: 'https://diadiemvietnam.vn/wp-content/uploads/2023/05/Mau-nail.jpg',
        subName: 'Red Color',
        price: 78.49,
        discount: 10.99,
      },
      {
        id: 2,
        name: 'Product 2',
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSs-pw1WnSwbOktSalkUDHxRB5OK1qvXoRyRENFTHzf4f9bSD3h9N10h_uv83eIG5bIMm4&usqp=CAU',
        subName: 'Red Color',
        price: 78.49,
        discount: 10.99,
      },
    ],
  },
  {
    id: 2,
    store: {
      id: 2,
      name: 'XYZ Nail Supply',
    },
    products: [
      {
        id: 3,
        name: 'Product 1',
        image: 'https://diadiemvietnam.vn/wp-content/uploads/2023/05/Mau-nail.jpg',
        subName: 'Red Color',
        price: 78.49,
        discount: 10.99,
      },
      {
        id: 4,
        name: 'Product 2',
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSs-pw1WnSwbOktSalkUDHxRB5OK1qvXoRyRENFTHzf4f9bSD3h9N10h_uv83eIG5bIMm4&usqp=CAU',
        subName: 'Red Color',
        price: 78.49,
        discount: 10.99,
      },
    ],
  },
];

export default CartScreen;
