export type ProductType = {
  id: number;
  name: string;
  image: string;
  subName: string;
  price: number;
  discount: number;
};

export type StoreType = {
  id: number;
  name: string;
};

export type CartType = {
  id: number;
  products: ProductType[];
  store: StoreType;
};
