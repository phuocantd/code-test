import { NavigationProp, useNavigation } from '@react-navigation/native';
import React, { useCallback } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { AppRoutes } from '~configs/constants';
import { tw } from '~configs/tw';
import { RootStackParamList } from '~navigation/type';

const HomeScreen = () => {
  const navigation = useNavigation<NavigationProp<RootStackParamList, AppRoutes.HOME>>();

  const onMyCart = useCallback(() => navigation.navigate(AppRoutes.CART), [navigation]);

  return (
    <View style={tw`flex-1 justify-center items-center`}>
      <TouchableOpacity onPress={onMyCart}>
        <Text>Cart</Text>
      </TouchableOpacity>
    </View>
  );
};

export default HomeScreen;
