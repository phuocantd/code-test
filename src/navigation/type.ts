import { AppRoutes } from '~configs/constants/routes';

export type RootStackParamList = {
  [AppRoutes.HOME]: undefined;
  [AppRoutes.ABOUT]: undefined;
  [AppRoutes.CART]: undefined;
};
